Readme:
         Restaurant Reservation System
-It is a simple restaurant reservation system that hold database for a restaurant
 including the users data(name,username,password,role) and restaurant's tables and dishes
it allows the user to login if he had signed in before ,if he wasn't he must login first
at signing in according to his role he will be directed to the suitable dashboard
*XML FILES FOR LOADING MUST BE IN THE SAME DIRECTORY AS THE JAR FILE
-according to work divission:

Farah's work:
1-design of the user's classes
2-reading users,tables,dishes from the xml file
3-adding users to the xml file
4-implementing the main method,Users'classes(inheriting from user 4 classes:client,manager,waiter,cooker)
implementing dishes classes (inheriting from dishes 3 classes:appetizer,maincourse,desert)
5-making the manager dashboard ,waiter dashboard and cooks dashboard
6-testing,debugging and editing users,tables,dishes classes

Mohamed's work:
1- design of the dishes classes and reservation classes  
2-reading reservations from xml file
3-adding reservations to the xml file
4-implementing reservation classes(DishesAndQuantity,Reservation)
 and the algorithm of calling them in the customer's reservatin gui
5-making the main(login/signup) screen and the reservation screen
6-testing,debugging and editing reservations classes