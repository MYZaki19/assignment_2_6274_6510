import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "table")
@XmlAccessorType(XmlAccessType.FIELD)
public class TableXML {
    @XmlElement(name = "number")
    private int table_number;
    @XmlElement(name = "number_of_seats")
    private int no_seats;
    @XmlElement(name = "smoking")
    private Boolean smoking;

    private Boolean is_occupied = false;

    public TableXML() {
    }

    public int getTable_number() {
        return table_number;
    }

    public void setTable_number(int table_number) {
        this.table_number = table_number;
    }

    public int getNo_seats() {
        return no_seats;
    }

    public void setNo_seats(int no_seats) {
        this.no_seats = no_seats;
    }

    public Boolean getSmoking() {
        return smoking;
    }

    public void setSmoking(Boolean smoking) {
        this.smoking = smoking;
    }

    public Boolean getIs_occupied() {
        return is_occupied;
    }

    public void setIs_occupied(Boolean is_occupied) {
        this.is_occupied = is_occupied;
    }
}
