import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name = "users")
@XmlAccessorType(XmlAccessType.FIELD)
public class Users {
    @XmlElement(name = "user")
    private ArrayList<UserXML> user = null;

    public Users() {
    }

    public ArrayList<UserXML> getUser() {
        return user;
    }

    public void setUser(ArrayList<UserXML> user) {
        this.user = user;
    }
}
