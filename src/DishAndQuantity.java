import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "dish and quantity")
@XmlAccessorType(XmlAccessType.FIELD)
public class DishAndQuantity {
    private String dish;
    private int  quantity;

    DishAndQuantity() {
    }

    DishAndQuantity(String dish, int quantity){
        this.dish = dish;
        this.quantity = quantity;
    }
    public String getDish() {return dish;}
    public int getQuantity() {return quantity;}
}
