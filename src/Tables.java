import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name = "tables")
@XmlAccessorType(XmlAccessType.FIELD)
public class Tables {
    @XmlElement(name = "table")
    private ArrayList<TableXML> tableXML = null;

    public ArrayList<TableXML> getTableXML() {
        return tableXML;
    }

    public void setTableXML(ArrayList<TableXML> tableXML) {
        this.tableXML = tableXML;
    }
}
