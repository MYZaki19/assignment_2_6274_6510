public class MainCourse extends Dish {
    public MainCourse(String name, double price) {
        super(name, price);
        super.setTax(1.15);
    }
}
