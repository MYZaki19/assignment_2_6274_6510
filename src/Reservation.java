import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name = "reservation")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reservation {
    private int table_number;
    private String customerName;
    private double totalPrice;

    Reservation() {
    }

    @XmlElement(name = "dishes")
    private ArrayList<DishAndQuantity> dishes = new ArrayList<DishAndQuantity>();

    public void setDishes(ArrayList<DishAndQuantity> dishes) {
        this.dishes = dishes;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    Reservation(TableXML tableXML, String customerName) {
        this.table_number = tableXML.getTable_number();
        this.customerName = customerName;
    }

    public int getTable_number() {
        return table_number;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void addDish(Dish dish, int quantity) {
        dishes.add(new DishAndQuantity(dish.getName(), quantity));
    }

    public ArrayList<DishAndQuantity> getDishes() {
        return dishes;
    }
}
