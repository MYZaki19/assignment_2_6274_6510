import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;


public class Main extends Application {
    int reservationCounter = -1;
    User currentUser = null;
    Reservations resXML = new Reservations();


    public static void main(String[] args) {
        launch(args);
        // for saving the file
        /*Marshaller marshaller = jaxbContext.createMarshaller();
        RestaurantXML saved = new RestaurantXML();
        Users users = new Users();
        UserXML newuser = new UserXML();
        newuser.setName("farah");
        newuser.setUsername("farah");
        newuser.setRole("guest");
        newuser.setPassword("12");
        userx.add(newuser);
        users.setUsers(userx);
        saved.setUsers(users);
        marshaller.marshal(saved, new File("C:\\Users\\DELL\\IdeaProjects\\night1\\src\\save.xml"));*/
    }

    @Override
    public void start(Stage primaryStage) throws JAXBException {

        File resFile = new File("reservations.xml");
        if(resFile.exists()) {
            JAXBContext jaxbContext2 = JAXBContext.newInstance(Reservations.class);
            Unmarshaller jaxbUnmarshaller1 = jaxbContext2.createUnmarshaller();
            resXML = (Reservations) jaxbUnmarshaller1.unmarshal(resFile);
        }
        else {
            resXML.setReservation(new ArrayList<>());
        }

        ArrayList<Reservation> reservations = resXML.getReservation();

        JAXBContext jaxbContext = JAXBContext.newInstance(RestaurantXML.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        RestaurantXML restaurant = (RestaurantXML) unmarshaller.unmarshal(new File("file1.xml"));

        ArrayList<UserXML> userx = restaurant.getUsers().getUser();
        ArrayList<TableXML> tableList = restaurant.getTables().getTableXML();

        ArrayList<Dish> dishList = new ArrayList<>();
        for (DishXML d : restaurant.getDishes().getDish()) {
            switch (d.getType()) {
                case "appetizer":
                    Appetizer a = new Appetizer(d.getName(), d.getPrice());
                    dishList.add(a);
                    break;
                case "main_course":
                    MainCourse m = new MainCourse(d.getName(), d.getPrice());
                    dishList.add(m);
                    break;
                case "desert":
                    Desert de = new Desert(d.getName(), d.getPrice());
                    dishList.add(de);
                    break;
            }
        }

        primaryStage.setTitle("Restaurant Management System");


        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text welcomeMessage = new Text("Welcome! Please enter your details in the fields below:");
        welcomeMessage.setFont(Font.font("Helvetica", FontWeight.NORMAL, 20));
        grid.add(welcomeMessage, 0, 0, 4, 1);

        Label username = new Label("Username:");
        grid.add(username, 0, 5);

        TextField usernameTextField = new TextField();
        grid.add(usernameTextField, 1, 5);

        Label password = new Label("Password:");
        grid.add(password, 0, 6);

        PasswordField passwordField = new PasswordField();
        grid.add(passwordField, 1, 6);

        Button signIn = new Button("Sign In");
        Button signUp = new Button("Sign Up");
        HBox hBoxButton = new HBox(10);
        hBoxButton.setAlignment(Pos.BOTTOM_RIGHT);
        hBoxButton.getChildren().add(signIn);
        grid.add(hBoxButton, 1, 7);
        grid.add(signUp, 2, 7);

        Label signInFailed = new Label("Error. Please check your username/password. If you do not have an account, please sign up first.");
        signInFailed.setTextFill(Color.web("#ff0000", 0.8));
        signInFailed.setVisible(false);
        grid.add(signInFailed, 0, 9, 4, 1);


        //remove later:
        //grid.setGridLinesVisible(true);

        GridPane gridSignUp = new GridPane();
        gridSignUp.setAlignment(Pos.TOP_LEFT);
        gridSignUp.setHgap(10);
        gridSignUp.setVgap(10);
        gridSignUp.setPadding(new Insets(25, 25, 25, 25));

        //for the sign up screen
        Text signUpText = new Text("Please enter the following details below:");
        signUpText.setFont(Font.font("Helvetica", FontWeight.NORMAL, 20));
        gridSignUp.add(signUpText, 0, 0, 4, 1);
        Label userCheck = new Label("Account Type:");
        gridSignUp.add(userCheck, 0, 1);

        ComboBox<String> userClass = new ComboBox<>();
        userClass.getItems().addAll("Guest", "Chef", "Waiter", "Manager");
        gridSignUp.add(userClass, 1, 1);

        Label fullName = new Label("Full Name:");
        gridSignUp.add(fullName, 0, 3);
        TextField fullNameField = new TextField();
        gridSignUp.add(fullNameField, 1, 3);

        Label usernameSignUp = new Label("Username:");
        gridSignUp.add(usernameSignUp, 0, 5);
        TextField usernameSignUpField = new TextField();
        gridSignUp.add(usernameSignUpField, 1, 5);

        Label passwordSignUp = new Label("Password:");
        gridSignUp.add(passwordSignUp, 0, 7);
        PasswordField passwordSignUpField = new PasswordField();
        gridSignUp.add(passwordSignUpField, 1, 7);

        Button createAccount = new Button("Finished");
        HBox hBoxButton2 = new HBox(10);
        hBoxButton2.setAlignment(Pos.CENTER_RIGHT);
        hBoxButton2.getChildren().add(createAccount);
        gridSignUp.add(hBoxButton2, 1, 11);

        Button cancelSignUp = new Button("Cancel");
        gridSignUp.add(cancelSignUp, 1, 11);

        Scene mainMenu = new Scene(grid, 1280, 720);
        Scene signUpScreen = new Scene(gridSignUp, 1280, 720);

        primaryStage.setScene(mainMenu);
        primaryStage.show();

        //Waiter Dashboard
        GridPane gridWaiter = new GridPane();

        Text waiterTitle = new Text("WAITER DASHBOARD");
        waiterTitle.setFont(Font.font(50));
        gridWaiter.add(waiterTitle, 0, 0);

        Text welcomeWaiter = new Text();
        welcomeWaiter.setFont(Font.font(50));
        gridWaiter.add(welcomeWaiter, 0, 1);

        TreeItem<String> ROOT = new TreeItem<>("reservations list");
        ROOT.setExpanded(true);
        TreeView<String> tree = new TreeView<>(ROOT);
        gridWaiter.add(tree, 0, 10);

        Button wLogout = new Button("Log Out");
        wLogout.setFont(Font.font(35));
        gridWaiter.add(wLogout, 0, 50);

        //Chef Dashboard
        GridPane gridChef = new GridPane();

        Label chefLabel = new Label("CHEF DASHBOARD");
        chefLabel.setFont(Font.font(60));
        gridChef.add(chefLabel, 0, 0);

        Label welcomeChef = new Label();
        welcomeChef.setFont(Font.font(50));
        gridChef.add(welcomeChef, 0, 1);

        Button cLogout = new Button("Log Out");
        cLogout.setFont(Font.font(35));
        gridChef.add(cLogout, 0, 50);

        TreeItem<String> rootChef = new TreeItem<>("reservations list");
        rootChef.setExpanded(true);
        TreeView<String> treeChef = new TreeView<>(rootChef);
        gridChef.add(treeChef, 0, 10);

        //Manager Dashboard
        GridPane gridManager = new GridPane();

        Label managerTitle = new Label("MANAGER DASHBOARD");
        managerTitle.setFont(Font.font(40));
        gridManager.add(managerTitle, 0, 0);

        Label welcomeManager = new Label();
        welcomeManager.setFont(Font.font(35));
        gridManager.add(welcomeManager, 0, 1);

        Button m_logout = new Button("Log Out");
        HBox hBoxMLogOut = new HBox(10);
        hBoxMLogOut.setAlignment(Pos.BASELINE_LEFT);
        hBoxMLogOut.getChildren().add(m_logout);
        gridManager.add(hBoxMLogOut, 1, 50);

        Label label1 = new Label("employees data");
        label1.setFont(new Font("Arial", 20));
        gridManager.add(label1, 0, 2);
        TreeItem<String> employeesitems = new TreeItem<>(" employees");
        employeesitems.setExpanded(true);
        TreeItem<String> guestitems = new TreeItem<>(" Guests");
        guestitems.setExpanded(true);
        TreeView<String> empyeetree = new TreeView<>(employeesitems);
        gridManager.add(empyeetree, 0, 5);
        TreeView<String> guesttree = new TreeView<>(guestitems);
        //gridManager.add(empyeetree, 0, 10);
        gridManager.add(guesttree, 1, 5);
        TreeItem<String> resitems = new TreeItem<>("reservations");
        resitems.setExpanded(true);
        TreeView<String> restree = new TreeView<>(resitems);
        gridManager.add(restree, 3, 5);


        TextArea income = new TextArea();
        income.setEditable(false);


        gridManager.add(income, 7, 5);

        //customer dashboard
        AnchorPane anchorPaneGuest = new AnchorPane();

        Button logOut = new Button("Sign Out");
        logOut.setLayoutX(1201);
        logOut.setLayoutY(673);
        anchorPaneGuest.getChildren().add(logOut);

        Text welcomeUser = new Text();
        welcomeUser.setLayoutX(14.0);
        welcomeUser.setLayoutY(36.0);
        welcomeUser.setFont(Font.font("Helvetica", FontWeight.NORMAL, 24));
        welcomeUser.setWrappingWidth(460);

        anchorPaneGuest.getChildren().add(welcomeUser);

        Label seatsPrompt = new Label("Please enter the number of required seats:");
        seatsPrompt.setLayoutX(14.0);
        seatsPrompt.setLayoutY(83.0);
        seatsPrompt.prefHeight(272);
        seatsPrompt.prefWidth(16.0);
        seatsPrompt.setFont(Font.font("Helvetica", FontWeight.NORMAL, 14.0));

        anchorPaneGuest.getChildren().add(seatsPrompt);

        TextField noSeats = new TextField();
        noSeats.setPromptText("No. of Seats");
        noSeats.setLayoutX(14.0);
        noSeats.setLayoutY(99.0);
        noSeats.setPrefHeight(26);
        noSeats.setPrefWidth(84);

        anchorPaneGuest.getChildren().add(noSeats);

        Label smoking = new Label("Smoking?");
        smoking.setLayoutX(14.0);
        smoking.setLayoutY(141.0);
        smoking.setPrefHeight(16);
        smoking.setPrefWidth(272);
        seatsPrompt.setFont(Font.font("Helvetica", FontWeight.NORMAL, 14.0));
        anchorPaneGuest.getChildren().add(smoking);

        ToggleGroup smokingGrp = new ToggleGroup();

        RadioButton noSmoking = new RadioButton("No");
        noSmoking.setLayoutX(14.0);
        noSmoking.setLayoutY(163.0);


        RadioButton yesSmoking = new RadioButton("Yes");
        yesSmoking.setLayoutX(14.0);
        yesSmoking.setLayoutY(181.0);

        noSmoking.setToggleGroup(smokingGrp);
        yesSmoking.setToggleGroup(smokingGrp);

        noSmoking.setSelected(true);
        yesSmoking.setSelected(false);

        anchorPaneGuest.getChildren().add(noSmoking);
        anchorPaneGuest.getChildren().add(yesSmoking);

        Label smokingBad = new Label("Warning! Smoking is bad for your and other's health.");
        smokingBad.setLayoutX(14.0);
        smokingBad.setLayoutY(206.0);
        smokingBad.setPrefHeight(15);
        smokingBad.setPrefWidth(265);
        smokingBad.setFont(Font.font("Helvetica", FontWeight.NORMAL, 11.0));
        smokingBad.setTextFill(Color.web("#ff00009c"));
        smokingBad.setUnderline(true);
        smokingBad.setVisible(false);
        anchorPaneGuest.getChildren().add(smokingBad);

        smokingGrp.selectedToggleProperty().addListener((observableValue, toggle, t1) -> {
            RadioButton radioButton = (RadioButton) smokingGrp.getSelectedToggle();

            if (radioButton.getText().equals("Yes")) {
                smokingBad.setVisible(true);
            } else {
                smokingBad.setVisible(false);
            }
        });

        Button confirmTable = new Button("Confirm");
        confirmTable.setLayoutX(14.0);
        confirmTable.setLayoutY(230.0);
        anchorPaneGuest.getChildren().add(confirmTable);


        Text isTableFound = new Text("");
        isTableFound.setLayoutX(14.0);
        isTableFound.setLayoutY(290.0);
        isTableFound.setWrappingWidth(261);
        isTableFound.setFont(Font.font(14.0));
        anchorPaneGuest.getChildren().add(isTableFound);

        confirmTable.setOnAction(event -> {
            if (tableList.isEmpty() || noSeats.getText().equals("")) {
                isTableFound.setText("No table found. Please try again.");
            } else {
                for (TableXML table : tableList) {
                    if (!table.getIs_occupied()) {
                        if (table.getNo_seats() >= Integer.parseInt(noSeats.getText())) {
                            RadioButton radioButton = (RadioButton) smokingGrp.getSelectedToggle();
                            if ((table.getSmoking() && radioButton.getText().equals("Yes")) || (!table.getSmoking() && radioButton.getText().equals("No"))) {


                                isTableFound.setText("Table Found");
                                Text tableNo = new Text("");
                                tableNo.setLayoutX(14.0);
                                tableNo.setLayoutY(307.0);
                                tableNo.setWrappingWidth(129);
                                tableNo.setFont(Font.font("Helvetica", 14));
                                tableNo.setText("Table Number: " + table.getTable_number());
                                anchorPaneGuest.getChildren().add(tableNo);


                                Label dishPrompt = new Label("Please pick one or more dishes: (Use CTRL)");
                                dishPrompt.setLayoutX(14.0);
                                dishPrompt.setLayoutY(329.0);
                                dishPrompt.setPrefHeight(16.0);
                                dishPrompt.setPrefWidth(282.0);
                                dishPrompt.setFont(Font.font("Helvetica", 14));
                                anchorPaneGuest.getChildren().add(dishPrompt);

                                ListView<Dish> dishSelector = new ListView<>();
                                dishSelector.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
                                dishSelector.setMaxHeight(320);
                                dishSelector.prefHeight(320);
                                dishSelector.prefWidth(248);
                                dishSelector.setLayoutX(14.0);
                                dishSelector.setLayoutY(345.0);

                                dishSelector.setCellFactory(param -> new ListCell<Dish>() {
                                    @Override
                                    protected void updateItem(Dish dish, boolean empty) {
                                        super.updateItem(dish, empty);

                                        if (empty || dish == null || dish.getName() == null) {
                                            setText(null);
                                        } else {
                                            setText(dish.getName() + "\n" + dish.getPrice());
                                        }
                                    }
                                });
                                for (Dish dish : dishList) {
                                    dishSelector.getItems().add(dish);
                                }

                                anchorPaneGuest.getChildren().add(dishSelector);


                                Button confirmDishes = new Button("Confirm Dishes");
                                confirmDishes.setLayoutX(14.0);
                                confirmDishes.setLayoutY(678.0);
                                anchorPaneGuest.getChildren().add(confirmDishes);


                                logOut.setOnAction(event12 -> {
                                    noSeats.setText("");
                                    isTableFound.setText("");
                                    noSmoking.setSelected(true);
                                    anchorPaneGuest.getChildren().removeAll(tableNo, dishPrompt, dishSelector, confirmDishes);
                                    primaryStage.setScene(mainMenu);
                                });

                                confirmDishes.setOnAction(event1 -> {

                                    confirmDishes.setDisable(true);

                                    ObservableList<Dish> selectedDishes;
                                    selectedDishes = dishSelector.getSelectionModel().getSelectedItems();

                                    ArrayList<Text> dishText = new ArrayList<>();
                                    ArrayList<Spinner<Integer>> dishSpinners = new ArrayList<>();

                                    int i = 0;
                                    //dishes are here
                                    for (Dish dish : selectedDishes) {
                                        dishText.add(new Text(dish.getName()));
                                        dishText.get(i).setLayoutX(350);
                                        dishText.get(i).setLayoutY(29 + (33 * i));
                                        dishText.get(i).setWrappingWidth(157);
                                        dishText.get(i).setFont(Font.font(14.0));
                                        anchorPaneGuest.getChildren().add(dishText.get(i));

                                        dishSpinners.add(new Spinner<>(1, 99, 1));
                                        dishSpinners.get(i).setLayoutX(615.0);
                                        dishSpinners.get(i).setLayoutY(10.0 + (36 * i));
                                        dishSpinners.get(i).prefHeight(26.0);
                                        dishSpinners.get(i).prefWidth(78.0);
                                        anchorPaneGuest.getChildren().add(dishSpinners.get(i));

                                        i++;

                                    }

                                    Button submit = new Button("Submit");
                                    submit.setLayoutX(950.0);
                                    submit.setLayoutY(11.0);
                                    anchorPaneGuest.getChildren().add(submit);

                                    Button confirm = new Button("Confirm");
                                    confirm.setLayoutX(950.0);
                                    confirm.setLayoutY(50.0);
                                    confirm.setVisible(false);
                                    anchorPaneGuest.getChildren().add(confirm);

                                    int finalI1 = i;
                                    logOut.setOnAction(event22 -> {
                                        noSeats.setText("");
                                        isTableFound.setText("");
                                        noSmoking.setSelected(true);
                                        anchorPaneGuest.getChildren().removeAll(tableNo, dishPrompt, dishSelector, confirmDishes, submit, confirm);

                                        int i2;
                                        for (i2 = 0; i2 < finalI1; i2++) {
                                            anchorPaneGuest.getChildren().remove(dishText.get(i2));
                                            anchorPaneGuest.getChildren().remove(dishSpinners.get(i2));
                                        }

                                        primaryStage.setScene(mainMenu);
                                    });

                                    TextField priceCalc = new TextField();
                                    priceCalc.setEditable(false);
                                    priceCalc.setLayoutX(1022);
                                    priceCalc.setLayoutY(11);
                                    priceCalc.setPrefWidth(245);
                                    priceCalc.setPrefHeight(26);

                                    int finalI = i;
                                    confirm.setOnAction(event11 -> {
                                        reservations.add(new Reservation(table, currentUser.getName()));
                                        table.setIs_occupied(true);

                                        double totalPrice = Double.parseDouble(priceCalc.getText().substring(13));
                                        /*The TreeItems are added here*/
                                        TreeItem<String> res_c = new TreeItem<>("-->table no:" + table.getTable_number() + "\n");
                                        rootChef.getChildren().add(res_c);
                                        TreeItem<String> res_list = new TreeItem<>("customer name:" + currentUser.getName());
                                        TreeItem<String> res_info = new TreeItem<>("-->table no:" + table.getTable_number());
                                        resitems.getChildren().add(res_list);
                                        res_list.getChildren().add(res_info);

                                        reservationCounter++;
                                        int i1 = 0;
                                        for (Dish dish : selectedDishes) {
                                            reservations.get(reservationCounter).addDish(dish, dishSpinners.get(i1).getValue());
                                            TreeItem<String> res_dish = new TreeItem<>("dishes:" + dish.getName() + " x " + dishSpinners.get(i1).getValue() + "\n");
                                            TreeItem<String> res_m_dish = new TreeItem<>("dishes:" + dish.getName() + " x " + dishSpinners.get(i1).getValue() + "\n");
                                            res_info.getChildren().add(res_m_dish);
                                            res_c.getChildren().add(res_dish);
                                            i1++;
                                        }
                                        TreeItem<String> res_m_price = new TreeItem<>("Total: " + totalPrice);
                                        res_info.getChildren().add(res_m_price);

                                        reservations.get(reservationCounter).setTotalPrice(totalPrice);

                                        //waiter reservation addition
                                        TreeItem<String> res_w = new TreeItem<>("-->table no:" + table.getTable_number());
                                        TreeItem<String> res_w_name = new TreeItem<>("customer name:" + currentUser.getName());
                                        ROOT.getChildren().add(res_w);
                                        res_w.getChildren().add(res_w_name);

                                        Alert reserved = new Alert(Alert.AlertType.INFORMATION);
                                        reserved.setTitle("Reservation Successful");
                                        reserved.setHeaderText(null);
                                        reserved.setContentText("Reservation Successful");
                                        reserved.showAndWait();

                                        primaryStage.setScene(mainMenu);

                                        // destroy previous GUI
                                        noSeats.setText("");
                                        isTableFound.setText("");
                                        noSmoking.setSelected(true);
                                        anchorPaneGuest.getChildren().removeAll(tableNo, dishPrompt, dishSelector, confirmDishes, submit, confirm, priceCalc);

                                        int i2;
                                        for (i2 = 0; i2 < finalI; i2++) {
                                            anchorPaneGuest.getChildren().remove(dishText.get(i2));
                                            anchorPaneGuest.getChildren().remove(dishSpinners.get(i2));
                                        }

                                    });


                                    int finalI2 = i;
                                    submit.setOnAction(event2 -> {
                                        double totalPrice = 0;
                                        int j = 0;
                                        for (Dish dish : selectedDishes) {
                                            double price = dish.getPrice();
                                            int quantity = dishSpinners.get(j).getValue();
                                            double tax = dish.getTax();
                                            double total = (price * tax) * quantity;
                                            totalPrice += total;
                                            j++;
                                        }

                                        priceCalc.setText("Total + Tax: " + totalPrice);
                                        confirm.setVisible(true);
                                        anchorPaneGuest.getChildren().add(priceCalc);

                                        logOut.setOnAction(event3 -> {
                                            noSeats.setText("");
                                            isTableFound.setText("");
                                            noSmoking.setSelected(true);
                                            anchorPaneGuest.getChildren().removeAll(tableNo, dishPrompt, dishSelector, confirmDishes, submit, confirm, priceCalc);
                                            int i2;
                                            for (i2 = 0; i2 < finalI2; i2++) {
                                                anchorPaneGuest.getChildren().remove(dishText.get(i2));
                                                anchorPaneGuest.getChildren().remove(dishSpinners.get(i2));
                                            }

                                            primaryStage.setScene(mainMenu);
                                        });
                                    });

                                });

                                break;
                            }
                        }
                    }
                    //table not found
                    isTableFound.setText("No table found. Please try again.");
                }
            }
        });

        logOut.setOnAction(event -> {
            noSeats.setText("");
            noSmoking.setSelected(true);
            primaryStage.setScene(mainMenu);
        });


        ArrayList<User> userList = new ArrayList<>();
        for (UserXML s : userx) {
            switch (s.getRole()) {
                case "Manager":
                    User m = new Manager(s.getName(), s.getUsername(), s.getPassword());
                    userList.add(m);
                    TreeItem<String> mang_list = new TreeItem<>("employee:" + m.getName());
                    employeesitems.getChildren().add(mang_list);
                    TreeItem<String> managerinfo = new TreeItem<>("user name:" + m.getUsername());
                    mang_list.getChildren().add(managerinfo);
                    break;
                case "Guest":
                    User c = new Guest(s.getName(), s.getUsername(), s.getPassword());
                    userList.add(c);
                    TreeItem<String> guestlist = new TreeItem<>("guest:" + c.getName());
                    guestitems.getChildren().add(guestlist);
                    TreeItem<String> guestinfo = new TreeItem<>("user name:" + c.getUsername());
                    guestlist.getChildren().add(guestinfo);
                    break;
                case "Waiter":
                    User w = new Waiter(s.getName(), s.getUsername(), s.getPassword());
                    userList.add(w);
                    TreeItem<String> emp_waiter = new TreeItem<>("employee:" + w.getName());
                    employeesitems.getChildren().add(emp_waiter);
                    TreeItem<String> waiterinfo = new TreeItem<>("user name:" + w.getUsername());
                    emp_waiter.getChildren().add(waiterinfo);
                    break;
                case "Chef":
                    User k = new Chef(s.getName(), s.getUsername(), s.getPassword());
                    userList.add(k);
                    TreeItem<String> emp_chef = new TreeItem<>("employee:" + k.getName());
                    employeesitems.getChildren().add(emp_chef);
                    TreeItem<String> chefinfo = new TreeItem<>("user name:" + k.getUsername());
                    emp_chef.getChildren().add(chefinfo);
                    break;
            }
        }

        //end of customer dashboard
        Scene customerDashboardScene = new Scene(anchorPaneGuest, 1280, 720);
        Scene waiterDashboardScene = new Scene(gridWaiter, 800, 720);
        Scene chefDashboardScene = new Scene(gridChef, 800, 720);
        Scene managerDashboardScene = new Scene(gridManager, 1280, 720);
        signIn.setOnAction(event -> {

            boolean userFound = false;

            for (User user : userList) {
                if (usernameTextField.getText().equals(user.getUsername())) {
                    if (passwordField.getText().equals(user.getPassword())) {
                        userFound = true;
                        signInFailed.setVisible(false);
                        usernameTextField.setText("");
                        passwordField.setText("");
                        //sign in code here
                        String userType = String.valueOf(user.getClass());
                        switch (userType) {
                            case "class Guest":
                                currentUser = user;
                                welcomeUser.setText("Welcome, " + user.getName());
                                primaryStage.setScene(customerDashboardScene);
                                break;
                            case "class Waiter":
                                welcomeWaiter.setText("Welcome, " + user.getName());
                                primaryStage.setScene(waiterDashboardScene);
                                break;
                            case "class Chef":
                                welcomeChef.setText("Welcome, " + user.getName());
                                primaryStage.setScene(chefDashboardScene);
                                break;
                            case "class Manager": {
                                double total_income = 0.0;
                                for (Reservation reservation : reservations) {
                                    total_income += reservation.getTotalPrice();
                                }
                                income.setText("DAILY INCOME= " + total_income);
                                welcomeManager.setText("Welcome, " + user.getName());
                                primaryStage.setScene(managerDashboardScene);
                                break;
                            }
                            default:

                        }
                        break;
                    }
                }
            }
            if (!userFound) {
                signInFailed.setVisible(true);
            }

        });


        //sign up screen display
        signUp.setOnAction(actionEvent -> primaryStage.setScene(signUpScreen));

        //cancelling sign up
        cancelSignUp.setOnAction(actionEvent -> {
            userClass.setValue("");
            fullNameField.setText("");
            usernameSignUpField.setText("");
            passwordSignUpField.setText("");


            primaryStage.setScene(mainMenu);
        });
        wLogout.setOnAction(event -> primaryStage.setScene(mainMenu));
        cLogout.setOnAction(event -> primaryStage.setScene(mainMenu));
        m_logout.setOnAction(event -> primaryStage.setScene(mainMenu));
        //create account button
        createAccount.setOnAction(actionEvent -> {
            String userClassValue = userClass.getValue();
            switch (userClassValue) {
                case "Guest":
                    User guest = new Guest(fullNameField.getText(), usernameSignUpField.getText(), passwordSignUpField.getText());
                    TreeItem<String> guestlist = new TreeItem<>("guest:" + guest.getName());
                    guestitems.getChildren().add(guestlist);
                    TreeItem<String> guestinfo = new TreeItem<>("user name:" + guest.getUsername());
                    guestlist.getChildren().add(guestinfo);
                    userList.add(guest);
                    break;
                case "Chef":
                    User chef = new Chef(fullNameField.getText(), usernameSignUpField.getText(), passwordSignUpField.getText());
                    userList.add(chef);
                    TreeItem<String> emp_chef = new TreeItem<>("employee:" + chef.getName());
                    employeesitems.getChildren().add(emp_chef);
                    TreeItem<String> chefinfo = new TreeItem<>("user name:" + chef.getUsername());
                    emp_chef.getChildren().add(chefinfo);
                    break;
                case "Waiter":
                    User waiter = new Waiter(fullNameField.getText(), usernameSignUpField.getText(), passwordSignUpField.getText());
                    userList.add(waiter);
                    TreeItem<String> emp_waiter = new TreeItem<>("employee:" + waiter.getName());
                    employeesitems.getChildren().add(emp_waiter);
                    TreeItem<String> waiterinfo = new TreeItem<>("user name:" + waiter.getUsername());
                    emp_waiter.getChildren().add(waiterinfo);
                    break;
                case "Manager":
                    User manager = new Manager(fullNameField.getText(), usernameSignUpField.getText(), passwordSignUpField.getText());
                    userList.add(manager);
                    TreeItem<String> mang_list = new TreeItem<>("employee:" + manager.getName());
                    employeesitems.getChildren().add(mang_list);
                    TreeItem<String> managerinfo = new TreeItem<>("user name:" + manager.getUsername());
                    mang_list.getChildren().add(managerinfo);
                    break;
            }


            //registration successful popup
            Alert signUpSuccess = new Alert(Alert.AlertType.INFORMATION);
            signUpSuccess.setTitle("Registration Successful");
            signUpSuccess.setHeaderText(null);
            signUpSuccess.setContentText("Registration Successful");
            signUpSuccess.showAndWait();

            userClass.setValue("");
            fullNameField.setText("");
            usernameSignUpField.setText("");
            passwordSignUpField.setText("");

            primaryStage.setScene(mainMenu);

        });


        for(Reservation reservation: reservations){
            reservationCounter++;

            //loading item into employee dashboards
            TreeItem<String> res_c = new TreeItem<>("-->table no:" + reservation.getTable_number() + "\n");
            rootChef.getChildren().add(res_c);

            TreeItem<String> res_list = new TreeItem<>("customer name:" + reservation.getCustomerName());
            TreeItem<String> res_info = new TreeItem<>("-->table no:" + reservation.getTable_number());
            resitems.getChildren().add(res_list);
            res_list.getChildren().add(res_info);

            for(DishAndQuantity dishAndQuantity : reservation.getDishes())
            {
                TreeItem<String> res_dish = new TreeItem<>("dishes:" + dishAndQuantity.getDish() + " x " + dishAndQuantity.getQuantity() + "\n");
                res_c.getChildren().add(res_dish);
                res_info.getChildren().add(res_dish);
            }
            TreeItem<String> res_m_price = new TreeItem<>("Total: " + reservation.getTotalPrice());
            res_info.getChildren().add(res_m_price);

            TreeItem<String> res_w = new TreeItem<>("-->table no:" + reservation.getTable_number());
            TreeItem<String> res_w_name = new TreeItem<>("customer name:" + reservation.getCustomerName());
            ROOT.getChildren().add(res_w);
            res_w.getChildren().add(res_w_name);
        }

        primaryStage.setOnCloseRequest(event -> {

            ArrayList<DishXML> dishXMLArray = new ArrayList<>();
            for(Dish dish: dishList){
                DishXML dishXml = new DishXML(dish.getName(),dish.getPrice());
                switch(dish.getClass().toString()){
                    case "class MainCourse":
                        dishXml.setType("main_course");
                        break;
                    case "class Appetizer":
                        dishXml.setType("appetizer");
                        break;
                    case "class Desert":
                        dishXml.setType("desert");
                        break;
                }
                dishXMLArray.add(dishXml);
            }

            ArrayList<UserXML> userXMLArray = new ArrayList<>();
            for(User user: userList){
                UserXML userXml = new UserXML(user.getName(),user.getUsername(),user.getPassword());
                switch(user.getClass().toString()){
                    case "class Guest":
                        userXml.setRole("Guest");
                        break;
                    case "class Waiter":
                        userXml.setRole("Waiter");
                        break;
                    case "class Chef":
                        userXml.setRole("Chef");
                        break;
                    case "class Manager":
                        userXml.setRole("Manager");
                        break;
                }
                userXMLArray.add(userXml);
            }

            restaurant.getDishes().setDish(dishXMLArray);
            restaurant.getTables().setTableXML(tableList);
            restaurant.getUsers().setUser(userXMLArray);

            try {
                JAXBContext jaxbContext1 = JAXBContext.newInstance(Reservations.class);
                Marshaller jaxbMarshaller1 = jaxbContext1.createMarshaller();
                jaxbMarshaller1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbMarshaller1.marshal(resXML, new File("reservations.xml"));

                JAXBContext jaxbContext3 = JAXBContext.newInstance(RestaurantXML.class);
                Marshaller jaxbMarshaller2 = jaxbContext3.createMarshaller();
                jaxbMarshaller2.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbMarshaller2.marshal(restaurant, new File("file1.xml"));

            } catch (JAXBException e) {
                e.printStackTrace();
            }

        });
    }
}

