public class Appetizer extends Dish{
    public Appetizer(String name, Double price) {
        super(name, price);
        super.setTax(1.10);
    }
}
