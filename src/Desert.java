public class Desert extends Dish {
    public Desert(String name, double price) {
        super(name, price);
        super.setTax(1.20);
    }
}
