import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name = "dishes")
@XmlAccessorType(XmlAccessType.FIELD)
public class Dishes {
    @XmlElement(name = "dish")
    private ArrayList<DishXML> dish = null;

    public Dishes() {
    }

    public ArrayList<DishXML> getDish() {
        return dish;
    }

    public void setDish(ArrayList<DishXML> dish) {
        this.dish = dish;
    }
}
