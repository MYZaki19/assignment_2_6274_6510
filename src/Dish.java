abstract public class Dish {
    private String name;
    private double price;
    private double tax;

    Dish(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public Dish() {
    }

    public String getName() {
        return name;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
